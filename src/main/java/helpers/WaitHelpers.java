package helpers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class WaitHelpers {

    //Wait for Element Visible
    public void visible(WebDriver driver, WebElement element){

        try {

            WebDriverWait wait = new WebDriverWait(driver, 60);
            wait.until(ExpectedConditions.visibilityOf(element));

        }catch (Exception e){
            Assert.fail();
            new Exception(e);
        }
    }

}



