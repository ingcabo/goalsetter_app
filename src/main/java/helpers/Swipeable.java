package helpers;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;

public interface Swipeable extends Waitable{


    default void swipeVertical(double startPercentage, double finalPercentage, double anchorPercentage, int duration) throws Exception {
        Dimension size = getDriver().manage().window().getSize();
        int anchor = (int) (size.width * anchorPercentage);
        int startPoint = (int) (size.height * startPercentage);
        int endPoint = (int) (size.height * finalPercentage);
        //new TouchAction((PerformsTouchActions) driver).press(anchor, startPoint).waitAction(Duration.ofMillis(duration)).moveTo(anchor, endPoint).release().perform();
    }

    default void drag(WebElement element, int xOffset, int yOffset) {

        waitForClickable(element);

        TouchAction drag=new TouchAction((AppiumDriver) getDriver());

        int startX=element.getLocation().getX();
        int startY=element.getLocation().getY();

        drag.press(PointOption.point(startX,startY)).moveTo(PointOption.point(xOffset,yOffset)).release().perform();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    default void drag(By element, int xOffset, int yOffset) {

        waitForClickable(element);
        WebElement ele = getDriver().findElement(element);

        TouchAction drag=new TouchAction((AppiumDriver) getDriver());

        int startX=ele.getLocation().getX();
        int startY=ele.getLocation().getY();

        drag.press(PointOption.point(startX,startY)).moveTo(PointOption.point(xOffset,yOffset)).release().perform();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}
