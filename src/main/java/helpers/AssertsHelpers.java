package helpers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class  AssertsHelpers {

    static helpers.WaitHelpers waitHelpers = new helpers.WaitHelpers();

    //Assert Equals
    public static void equals(String value1, String value2) {
        try {
            Assert.assertEquals(value1, value2);
        } catch (Exception e) {
            Assert.fail();
            new Exception(e);
        }
    }

    //Assert Exist
    public static void exist(WebDriver driver, WebElement element) {

        try {

            waitHelpers.visible(driver, element);

            boolean value = element.isDisplayed();
            Assert.assertTrue(value);

        } catch (Exception e) {
            Assert.fail();
            new Exception(e);
        }
    }

}
