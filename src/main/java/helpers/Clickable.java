package helpers;

import org.openqa.selenium.WebElement;
import org.testng.Assert;

public interface Clickable extends Waitable {

    //Click for element
    default void click(WebElement element){
        try {
            waitForVisible(element);
            element.click();
        }catch (Exception e){
            Assert.fail();
            new Exception(e);
        }
    }

}
