package helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public interface Waitable extends helpers.Driverable {

    WebDriverWait waiter();

    //Wait for Element Clickable
    default void waitForClickable(By element){
        try {
            waiter().until(ExpectedConditions.elementToBeClickable(element));
        } catch (Exception e){
            Assert.fail();
            new Exception(e);
        }
    }

    //Wait for Element Clickable
    default void waitForClickable(WebElement element){
            try {
                waiter().until(ExpectedConditions.elementToBeClickable(element));
            } catch (Exception e){
                Assert.fail();
                new Exception(e);
            }
    }

    //Wait for Element Visible
    default void waitForVisible(WebElement element){
        try {
            waiter().until(ExpectedConditions.visibilityOf(element));
        } catch (Exception e){
            Assert.fail();
            new Exception(e);
        }
    }

}
