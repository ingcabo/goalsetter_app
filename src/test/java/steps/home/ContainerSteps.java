package steps.home;

import helpers.AssertsHelpers;
import helpers.UtilHelpers;
import io.appium.java_client.MobileElement;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pom.home.ContainerPage;
import steps.BaseSteps;

public class ContainerSteps extends BaseSteps {

    ContainerPage containerPage = new ContainerPage(this.driver);

    //Validar firts child
    @And("I want to verify the first child")
    public void validate_firstchild() {

        //Validar que estamos en la home
        AssertsHelpers.exist(driver, containerPage.text_name);
    }

    // Scroll down
    @And("I scroll down to the second container on option {string}")
    public void iScrollDownToTheSecondContainerOnOption(String uiSelector) {

        driver.findElementByAndroidUIAutomator(UtilHelpers.uiScrollableUsingTextContains(uiSelector));
        System.out.println("Scroll down successful");
    }


    @When("I click on tab savings")
    public void onclickinsavings() {

        containerPage.click(containerPage.btn_savings);
        System.out.println("======>Direct to a page Savings successful");
    }

    @Then("I verify that the name is {string}")
    public void validate_header(String name) {

        String nameHeader = containerPage.Txt_Header_savings.getText();
        AssertsHelpers.equals(nameHeader, name);
    }

}