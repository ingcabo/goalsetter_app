package steps.wallet;

import helpers.AssertsHelpers;
import io.cucumber.java.en.Then;
import pom.wallet.WalletPage;
import steps.BaseSteps;

public class WalletSteps extends BaseSteps {

    WalletPage WalletPage = new WalletPage(this.driver);

    @Then("I enter {string} and validate balance {string}")
    public void iEnterAndValidateBalance(String headerText, String amountBalance) {

        waitHelpers.visible(driver, WalletPage.Text_Wallet);
        String textAmountBalance = WalletPage.text_mount.getText();
        String textHeader = WalletPage.Text_Wallet.getText();

        AssertsHelpers.equals(textHeader, headerText);
        AssertsHelpers.equals(textAmountBalance, amountBalance);
        AssertsHelpers.exist(driver, WalletPage.Icon_Home);

        WalletPage.click(WalletPage.Icon_Home);
        System.out.println("======>Back to Home");
    }
}