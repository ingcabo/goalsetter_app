#  automation App Android

the **automation App Android** allows us to execute test on android device,

### Starting 🚀
These instructions will allow you to get a copy of the project running on your local machine for development and testing purposes.

See Deployment to know how to deploy the project.

### Prerequisites 📋
What things do you need to install the software and how to install them.

- [java 11](https://....).
- [Maven](https://...).
- [android virtual device manager](https://....).


What things do you need to configure/run the test regression.

- Create an Android Emulator with AndroidStudio Application. It has to be called "Pixel_4x" With os 11
- install and activate the Open VPN on the device.
- Copy the .apk in the fallowing path "src/main/resources/app/android/app-universal-qa.apk"
- check or configure your device parameter in "src/main/resources/conf/config.xml"
- Start Server Appium v 1.21.0.
- Open IntelliJIDEA. Click in “Open” search the path where this project Goalsetter_APP was cloned and choose POM.xml and “Open” then Select “Open as Project ''.
- In this Project search the file src/test/java/TestRunner.java and click the right button and then click Run TestRunner.
- If everything is OK, the application will be running on the device.
- at the end of the test execution you will get the link of the report.

### Architecture description 📋

- Apk repository -> "src/main/resources/app/android/"
- device parameter -> "src/main/resources/conf/config.xml"
- features -> "src/test/resources/feature/"
- steps -> src/test/java/steps
- page object repository -> "src/main/java/pom"
- some utils functions ->"src/main/java/helpers/"
- runner -> "src/test/java/TestRunner.java"